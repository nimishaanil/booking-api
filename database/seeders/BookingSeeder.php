<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use DB;

class BookingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    
        $booking_array = [
              [
                
                'user_id'=> 3,
                'venue_id' => 1,
                'check_in' => '2024-01-02 20:00:00',
                'check_out' => '2024-01-02 21:00:00',
                'sport' => 'Football',
                'court_type' => '7x7',
                'price' => 1300.00,
                'final_price' => 800.00,
                'discount' => 500.00,
                'status' => 'Active',
                'created_at' => '2023-12-01 08:42:52',
                'updated_at' => '2023-12-01 08:42:52'
              ],
              [
                'user_id'=> 2,
                'venue_id' => 1,
                'check_in' => '2024-01-02 20:00:00',
                'check_out' => '2024-01-02 21:00:00',
                'sport' => 'Football',
                'court_type' => '7x7',
                'price' => 1300.00,
                'final_price' => 800.00,
                'discount' => 500.00,
                'status' => 'Active',
                'created_at' => '2023-12-01 08:42:52',
                'updated_at' => '2023-12-01 08:42:52'
              ],
              [
                'user_id'=> 1,
                'venue_id' => 1,
                'check_in' => '2024-01-09 20:00:00',
                'check_out' => '2024-01-09 21:00:00',
                'sport' => 'Cricket',
                'court_type' => 'Full Ground',
                'price' => 1500.00,
                'final_price' => 1000.00,
                'discount' => 500.00,
                'status' => 'Active',
                'created_at' => '2023-12-01 08:42:52',
                'updated_at' => '2023-12-01 08:42:52'
              ],
              [
                'user_id'=> 5,
                'venue_id' => 2,
                'check_in' => '2024-01-05 06:30:00',
                'check_out' => '2024-01-05 08:00:00',
                'sport' => 'Football',
                'court_type' => '7x7',
                'price' => 1300.00,
                'final_price' => 1300.00,
                'discount' => 0.00,
                'status' => 'Active',
                'created_at' => '2023-12-01 08:42:52',
                'updated_at' => '2023-12-01 08:42:52'
              ],
              [
                'user_id'=> 2,
                'venue_id' => 4,
                'check_in' => '2023-12-03 11:00:00',
                'check_out' => '2023-12-03 12:00:00',
                'sport' => 'Badminton',
                'court_type' => 'Court A',
                'price' => 1000.00,
                'final_price' => 800.00,
                'discount' => 200.00,
                'status' => 'Active',
                'created_at' => '2023-12-01 08:42:52',
                'updated_at' => '2023-12-01 08:42:52'
                ],
                [   
                    'user_id'=> 6,
                    'venue_id' => 1,
                    'check_in' => '2024-02-02 23:00:00',
                    'check_out' => '2024-02-03 01:30:00',
                    'sport' => 'Cricket',
                    'court_type' => '7x7',
                    'price' => 1500.00,
                    'final_price' => 1000.00,
                    'discount' => 500.00,
                    'status' => 'Active',
                    'created_at' => '2023-12-01 08:42:52',
                    'updated_at' => '2023-12-01 08:42:52'
                ],
                [
                    'user_id'=>7,
                    'venue_id' => 2,
                    'check_in' => '2024-01-02 21:00:00',
                    'check_out' => '2024-01-02 22:00:00',
                    'sport' => 'Football',
                    'court_type' => '7x7',
                    'price' => 1300.00,
                    'final_price' => 1300.00,
                    'discount' => 0.00,
                    'status' => 'Active',
                    'created_at' => '2023-12-01 08:42:52',
                    'updated_at' => '2023-12-01 08:42:52'
                ],
                [
                    'user_id'=>8,
                    'venue_id' => 2,
                    'check_in' => '2024-02-03 04:00:00',
                    'check_out' => '2024-02-03 05:00:00',
                    'sport' => 'Football',
                    'court_type' => '7x7',
                    'price' => 1200.00,
                    'final_price' => 1000.00,
                    'discount' => 100.00,
                    'status' => 'Active',
                    'created_at' => '2023-12-01 08:42:52',
                    'updated_at' => '2023-12-01 08:42:52'
                ],
                [
                    'user_id'=>1,
                    'venue_id' => 1,
                    'check_in' => '2024-03-03 08:00:00',
                    'check_out' => '2024-03-03 10:00:00',
                    'sport' => 'Cricket',
                    'court_type' => 'Nets',
                    'price' => 1500.00,
                    'final_price' => 1000.00,
                    'discount' => 500.00,
                    'status' => 'Active',
                    'created_at' => '2023-12-01 08:42:52',
                    'updated_at' => '2023-12-01 08:42:52'
                ],
                [
                    'user_id'=>9,
                    'venue_id' => 3,
                    'check_in' => '2024-01-03 07:30:00',
                    'check_out' => '2024-01-03 08:30:00',
                    'sport' => 'Football',
                    'court_type' => '7x7',
                    'price' => 1300.00,
                    'final_price' => 800.00,
                    'discount' => 500.00,
                    'status' => 'Active',
                    'created_at' => '2023-12-01 08:42:52',
                    'updated_at' => '2023-12-01 08:42:52'
                ],
                [
                    'user_id'=>10,
                    'venue_id' => 1,
                    'check_in' => '2024-01-03 05:00:00',
                    'check_out' => '2024-01-03 06:00:00',
                    'sport' => 'Cricket',
                    'court_type' => '7x7',
                    'price' => 900.00,
                    'final_price' => 800.00,
                    'discount' => 100.00,
                    'status' => 'Active',
                    'created_at' => '2023-12-01 08:42:52',
                    'updated_at' => '2023-12-01 08:42:52'
                ],
                [
                    'user_id'=>6,
                    'venue_id' => 1,
                    'check_in' => '2024-01-02 19:00:00',
                    'check_out' => '2024-01-02 20:00:00',
                    'sport' => 'Football',
                    'court_type' => '7x7',
                    'price' => 1300.00,
                    'final_price' => 1300.00,
                    'discount' => 0.00,
                    'status' => 'Active',
                    'created_at' => '2023-12-01 08:42:52',
                    'updated_at' => '2023-12-01 08:42:52'
                ],
                [
                    'user_id'=>1,
                    'venue_id' => 1,
                    'check_in' => '2024-01-02 17:00:00',
                    'check_out' => '2024-01-02 19:00:00',
                    'sport' => 'Badminton',
                    'court_type' => 'Court A',
                    'price' => 800.00,
                    'final_price' => 800.00,
                    'discount' => 0.00,
                    'status' => 'Active',
                    'created_at' => '2023-12-01 08:42:52',
                    'updated_at' => '2023-12-01 08:42:52'
                ],
                [
                
                    'user_id'=> 13,
                    'venue_id' => 5,
                    'check_in' => '2024-01-02 20:00:00',
                    'check_out' => '2024-01-02 21:00:00',
                    'sport' => 'Football',
                    'court_type' => '7x7',
                    'price' => 1300.00,
                    'final_price' => 800.00,
                    'discount' => 500.00,
                    'status' => 'Active',
                    'created_at' => '2023-12-01 08:42:52',
                    'updated_at' => '2023-12-01 08:42:52'
                  ],
                  [
                    'user_id'=> 12,
                    'venue_id' => 5,
                    'check_in' => '2024-01-02 20:00:00',
                    'check_out' => '2024-01-02 21:00:00',
                    'sport' => 'Football',
                    'court_type' => '7x7',
                    'price' => 1300.00,
                    'final_price' => 800.00,
                    'discount' => 500.00,
                    'status' => 'Cancelled',
                    'created_at' => '2023-12-01 08:42:52',
                    'updated_at' => '2023-12-01 08:42:52'
                  ],
                  [
                    'user_id'=> 1,
                    'venue_id' => 4,
                    'check_in' => '2024-01-09 20:00:00',
                    'check_out' => '2024-01-09 21:00:00',
                    'sport' => 'Cricket',
                    'court_type' => 'Full Ground',
                    'price' => 1500.00,
                    'final_price' => 1000.00,
                    'discount' => 500.00,
                    'status' => 'Active',
                    'created_at' => '2023-12-01 08:42:52',
                    'updated_at' => '2023-12-01 08:42:52'
                  ],
                  [
                    'user_id'=> 5,
                    'venue_id' => 4,
                    'check_in' => '2024-01-05 06:30:00',
                    'check_out' => '2024-01-05 08:00:00',
                    'sport' => 'Football',
                    'court_type' => '7x7',
                    'price' => 1300.00,
                    'final_price' => 1300.00,
                    'discount' => 0.00,
                    'status' => 'Cancelled',
                    'created_at' => '2023-12-01 08:42:52',
                    'updated_at' => '2023-12-01 08:42:52'
                  ],
                  [
                    'user_id'=> 2,
                    'venue_id' => 3,
                    'check_in' => '2023-12-03 11:00:00',
                    'check_out' => '2023-12-03 12:00:00',
                    'sport' => 'Badminton',
                    'court_type' => 'Court A',
                    'price' => 1000.00,
                    'final_price' => 800.00,
                    'discount' => 200.00,
                    'status' => 'Cancelled',
                    'created_at' => '2023-12-01 08:42:52',
                    'updated_at' => '2023-12-01 08:42:52'
                    ],
                    [   
                        'user_id'=> 6,
                        'venue_id' => 3,
                        'check_in' => '2024-02-02 23:00:00',
                        'check_out' => '2024-02-03 01:30:00',
                        'sport' => 'Cricket',
                        'court_type' => '7x7',
                        'price' => 1500.00,
                        'final_price' => 1000.00,
                        'discount' => 500.00,
                        'status' => 'Active',
                        'created_at' => '2023-12-01 08:42:52',
                        'updated_at' => '2023-12-01 08:42:52'
                    ],
                    [
                        'user_id'=>7,
                        'venue_id' => 2,
                        'check_in' => '2024-01-02 21:00:00',
                        'check_out' => '2024-01-02 22:00:00',
                        'sport' => 'Football',
                        'court_type' => '7x7',
                        'price' => 1300.00,
                        'final_price' => 1300.00,
                        'discount' => 0.00,
                        'status' => 'Active',
                        'created_at' => '2023-12-01 08:42:52',
                        'updated_at' => '2023-12-01 08:42:52'
                    ],
                    [
                        'user_id'=>8,
                        'venue_id' => 2,
                        'check_in' => '2024-02-03 04:00:00',
                        'check_out' => '2024-02-03 05:00:00',
                        'sport' => 'Football',
                        'court_type' => '7x7',
                        'price' => 1200.00,
                        'final_price' => 1000.00,
                        'discount' => 100.00,
                        'status' => 'Active',
                        'created_at' => '2023-12-01 08:42:52',
                        'updated_at' => '2023-12-01 08:42:52'
                    ],
                    [
                        'user_id'=>1,
                        'venue_id' => 1,
                        'check_in' => '2024-03-03 08:00:00',
                        'check_out' => '2024-03-03 10:00:00',
                        'sport' => 'Cricket',
                        'court_type' => 'Nets',
                        'price' => 1500.00,
                        'final_price' => 1000.00,
                        'discount' => 500.00,
                        'status' => 'Active',
                        'created_at' => '2023-12-01 08:42:52',
                        'updated_at' => '2023-12-01 08:42:52'
                    ],
                    [
                        'user_id'=>9,
                        'venue_id' => 3,
                        'check_in' => '2024-01-03 07:30:00',
                        'check_out' => '2024-01-03 08:30:00',
                        'sport' => 'Football',
                        'court_type' => '7x7',
                        'price' => 1300.00,
                        'final_price' => 800.00,
                        'discount' => 500.00,
                        'status' => 'Active',
                        'created_at' => '2023-12-01 08:42:52',
                        'updated_at' => '2023-12-01 08:42:52'
                    ],
                    [
                        'user_id'=>10,
                        'venue_id' => 1,
                        'check_in' => '2024-01-03 05:00:00',
                        'check_out' => '2024-01-03 06:00:00',
                        'sport' => 'Cricket',
                        'court_type' => '7x7',
                        'price' => 900.00,
                        'final_price' => 800.00,
                        'discount' => 100.00,
                        'status' => 'Active',
                        'created_at' => '2023-12-01 08:42:52',
                        'updated_at' => '2023-12-01 08:42:52'
                    ],
                    [
                        'user_id'=>6,
                        'venue_id' => 4,
                        'check_in' => '2024-01-02 19:00:00',
                        'check_out' => '2024-01-02 20:00:00',
                        'sport' => 'Football',
                        'court_type' => '7x7',
                        'price' => 1300.00,
                        'final_price' => 1300.00,
                        'discount' => 0.00,
                        'status' => 'Cancelled',
                        'created_at' => '2023-12-01 08:42:52',
                        'updated_at' => '2023-12-01 08:42:52'
                    ],
                    [
                        'user_id'=>1,
                        'venue_id' => 1,
                        'check_in' => '2024-01-02 17:00:00',
                        'check_out' => '2024-01-02 19:00:00',
                        'sport' => 'Badminton',
                        'court_type' => 'Court A',
                        'price' => 800.00,
                        'final_price' => 800.00,
                        'discount' => 0.00,
                        'status' => 'Active',
                        'created_at' => '2023-12-01 08:42:52',
                        'updated_at' => '2023-12-01 08:42:52'
                    ]
        ];

        DB::table('booking')->insert($booking_array);
    }
}
