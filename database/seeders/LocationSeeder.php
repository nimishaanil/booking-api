<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

use Faker\Factory as Faker;

use DB;

class LocationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $location_array = [
            [
                'name' => 'Super Soccer Stadium',
                'address' => '233 Wilson Trafficway',
                'created_at' => '2023-12-01 08:42:52',
                'updated_at' => '2023-12-01 08:42:52'
            ],
            [
                'name' => 'City Arena',
                'address' => '4169 Kub Gardens Apt. 038',
                'created_at' => '2023-12-01 08:42:52',
                'updated_at' => '2023-12-01 08:42:52'
            ],
            [
                'name' => 'Indoor Stadium',
                'address' => '623 Kihn Spring Apt. 170',
                'created_at' => '2023-12-01 08:42:52',
                'updated_at' => '2023-12-01 08:42:52'
            ],
            [
                'name' => 'Sports Hub',
                'address' => '197 Jast Flat Apt. 932',
                'created_at' => '2023-12-01 08:42:52',
                'updated_at' => '2023-12-01 08:42:52'
            ],
            [
                'name' => 'Game Time Arena',
                'address' => '50205 Daphne Dam Suite 798',
                'created_at' => '2023-12-01 08:42:52',
                'updated_at' => '2023-12-01 08:42:52'
            ]
            
        ];

        DB::table('location')->insert($location_array);
    }
}
