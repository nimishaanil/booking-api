<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('booking', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id'); 
            $table->unsignedBigInteger('venue_id');  
            $table->timestamp('check_in');
            $table->timestamp('check_out');
            $table->string('sport');
            $table->string('court_type');
            $table->float('price');
            $table->float('final_price');
            $table->float('discount');
            $table->string('status'); //Active, Cancelled
            $table->timestamps();


            $table->foreign('user_id')->references('id')->on('customer')->onDelete('cascade');
            $table->foreign('venue_id')->references('id')->on('location')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('booking');
    }
}
